<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMailToCustomer extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $dataUsers;
    public $dataC2b;
    public $dateTime;

    public function __construct($dataUsers, $dataC2b, $dateTime)
    {
        $this->dataUsers    = $dataUsers;
        $this->dataC2b       = $dataC2b;
        $this->dateTime     = $dateTime;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.c2bMail');
    }
}
