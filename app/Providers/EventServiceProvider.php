<?php

namespace App\Providers;

use App\Events\c2bcustomerHasPaidEvent;
use App\Listeners\messageCustomerPayListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        \App\Events\StkPushEvent::class => [
            \App\Listeners\StkPushEventListener::class
        ],
        \App\Events\C2bTransactionEvent::class => [
            \App\Listeners\C2bTransactionEventListener::class,
                \App\Listeners\C2bSendMailEventListener::class
        ],
        \App\Events\B2cTransactionEvent::class => [
            \App\Listeners\B2cTransactionEventListener::class
        ],
    ];


    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
