<?php

namespace App\Listeners;

use App\Mail\SendMailToCustomer;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class C2bSendMailEventListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $dataUsers = $event->user;
        $dataC2b = $event->data;
        $dateTime = Carbon::parse($dataC2b['TransTime'])->toDateTimeString();

        Mail::to($dataUsers->email)->send(new SendMailToCustomer($dataUsers, $dataC2b, $dateTime));
    }
}
