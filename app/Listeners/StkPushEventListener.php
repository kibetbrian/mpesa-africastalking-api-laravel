<?php

namespace App\Listeners;

use AfricasTalking\SDK\AfricasTalking;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\StkPushEvent;
use Carbon\Carbon;
use App\Http\Traits\SendSmsTrait;
use Illuminate\Support\Facades\Log;

class StkPushEventListener implements ShouldQueue
{
    use SendSmsTrait;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(StkPushEvent $event)
    {
        $data = $event->callBackMetaData;
        //Log::info($data);
        $dateTime = Carbon::parse($data[3]['Value'])->toDateTimeString();

        $message = $data[1]['Value'] . ' Confirmed Your payment of Ksh ' . $dateTime. ' Thank you.';
        $mobile = $data[4]['Value'];
        
        $this->sendSms($mobile, $message);
    }
}
