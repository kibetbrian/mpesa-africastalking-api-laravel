<?php

namespace App\Listeners;

use AfricasTalking\SDK\AfricasTalking;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use App\Http\Traits\SendSmsTrait;

class B2cTransactionEventListener implements ShouldQueue
{
    use SendSmsTrait;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $b2cTransaction = $event->b2c;

        //Log::info($b2cTransaction);

        list($mobile, $name) = explode('-', trim($b2cTransaction[5]['Value']));

        // Log:info($mobile);

        $message = 'Dear, ' .   $name . ' we have sent you Ksh ' . $b2cTransaction[1]['Value'] . ' on ' . $b2cTransaction[4]['Value'] . ' for the goods sold to us. Transaction Ref No' . $b2cTransaction[0]['Value'] . ' Thank you!';



        $this->sendSms($mobile, $message);
    }
}
