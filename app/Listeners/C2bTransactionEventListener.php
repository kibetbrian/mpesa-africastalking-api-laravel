<?php

namespace App\Listeners;

use AfricasTalking\SDK\AfricasTalking;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Http\Traits\SendSmsTrait;

class C2bTransactionEventListener implements ShouldQueue
{
    use SendSmsTrait;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        // Log:info($event->data);
        $c2bTransaction = $event->data;
        $dateTime = Carbon::parse($c2bTransaction['TransTime'])->toDateTimeString();

        $message = 'Dear ' . $c2bTransaction['FirstName'] . ', please note that your payment has been received Ref No ' . $c2bTransaction['TransID'] . ' of Ksh ' . $c2bTransaction['TransAmount'] . ' on ' .  $dateTime;
        $mobile = $c2bTransaction['MSISDN'];

       // Log:info($mobile);
        $this->sendSms($mobile, $message);
    }
}
