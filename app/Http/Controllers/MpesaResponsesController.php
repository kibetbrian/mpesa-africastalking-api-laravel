<?php

namespace App\Http\Controllers;

use App\B2cTransaction;
use App\Events\B2cTransactionEvent;
use App\Events\C2bTransactionEvent;
use App\Events\StkPushEvent;
use App\Mail\SendMailToCustomer;
use App\MpesaPayments;
use App\STKpush;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use PhpParser\Node\Stmt\Catch_;

class MpesaResponsesController extends Controller
{
    public function validationUrl(Request $request)
    {
        // Log::info('Validation endpoint hit');
        // Log::info($request->all());

        return [
            'ResultCode' => 0,
            'ResultDesc' => 'Accept Service',
            'ThirdPartyTransID' => rand(3000, 10000)
        ];
    }

    public function confirmationUrl(Request $request)
    {
        Log::info('Confirmation endpoint hit');
        //Log::info($request->all());

        $data = $request->all();
        Log::info($data);

        //dd($data);
        DB::beginTransaction();
        try {
            $c2bTransaction = MpesaPayments::create([
                'FirstName'          => $data['FirstName'],
                'MiddleName'         => $data['MiddleName'],
                'LastName'           => $data['LastName'],
                'TransactionType'    => $data['TransactionType'],
                'TransID'            => $data['TransID'],
                'TransTime'          => $data['TransTime'],
                'OrgAccountBalance'  => $data['OrgAccountBalance'],
                'BusinessShortCode'  => $data['BusinessShortCode'],
                'BillRefNumber'      => $data['BillRefNumber'],
                'MSISDN'             => $data['MSISDN'],
                'TransAmount'        => $data['TransAmount'],
             ]);
            DB::commit();
             $user = DB::table('users')
                     ->select('name', 'email')
                     ->where('phone', $data['MSISDN'])
                     ->first();

             event(new C2bTransactionEvent($data, $user));
        } catch (Exception $exception) {
            DB::rollBack();
            Log::info($exception);
        }



        return [
            'ResultCode' => 0,
            'ResultDesc' => 'Accept Service',
            'ThirdPartyTransID' => rand(3000, 10000)
        ];
    }

    public function callbackURL(Request $request){
        Log::info('Confirmation endpoint hit');

        $stk = $request->all();

        $callBackMetaData =  $stk['Body']['stkCallback']['CallbackMetadata']['Item'];
        // $set= $stk['Body']['stkCallback']['CallbackMetadata']['Item']['0'];

         Log::info($stk);
        //  exit();

        DB::beginTransaction();
        try {
            $lipa = new STKpush;
            $lipa->Amount                = $callBackMetaData[0]['Value'];
            $lipa->MpesaReceiptNumber    = $callBackMetaData[1]['Value'];
           // $lipa->Balance               = $callBackMetaData[2]['Name'];
            $lipa->TransactionDate       = $callBackMetaData[3]['Value'];
            $lipa->PhoneNumber           = $callBackMetaData[4]['Value'];

            $lipa->save();
            DB::commit();

            event(new StkPushEvent($lipa, $callBackMetaData));
            }
            catch(Exception $exception){
                DB::rollBack();
                Log::info($exception);
            }

  /*       STKpush::create([
            'Amount' => $stk->Body->stkCallback->CallbackMetadata[0]->Value,
            'MpesaReceiptNumber' => $stk->Body->stkCallback->CallbackMetadata[1]->Value,
            'Balance' => $stk->Body->stkCallback->CallbackMetadata[2]->Value,
            'TransactionDate' => $stk->Body->stkCallback->CallbackMetadata[3]->Value,
            'PhoneNumber' => $stk->Body->stkCallback->CallbackMetadata[4]->Value,
            ]); */

        Log::info($request->all());

        return [
            'ResultCode' => 0,
            'ResultDesc' => 'Accept Service',
            'ThirdPartyTransID' => rand(3000, 10000)
        ];
    }
    public function resultURL(Request $request)
    {
        Log::info('Result endpoint hit');

        $resultB2C = $request->all();

        $b2c = $resultB2C['Result']['ResultParameters']['ResultParameter'];

        DB::beginTransaction();
        try{
            $b2cTransaction = B2cTransaction::create([
                'TransactionReceipt'                     => $b2c[0]['Value'],
                'TransactionAmount'                      => $b2c[1]['Value'],
                'B2CWorkingAccountAvailableFunds'        => $b2c[2]['Value'],
                'B2CUtilityAccountAvailableFunds'        => $b2c[3]['Value'],
                'TransactionCompletedDateTime'           => $b2c[4]['Value'],
                'ReceiverPartyPublicName'                => $b2c[5]['Value'],
                'B2CChargesPaidAccountAvailableFunds'    => $b2c[6]['Value'],
                'B2CRecipientIsRegisteredCustomer'       => $b2c[7]['Value']
            ]);
            DB::commit();

            event(new B2cTransactionEvent($b2c));
        }
        catch(Exception $exception){
        DB::rollBack();
        Log::info($exception);
        }



        //event(new B2cPaymentEvents($b2c, $b2cTransaction));


/*          $b2cTransaction = new b2cTransaction;

        $b2cTransaction->TransactionReceipt                     = $b2c[0]['Value'];
        $b2cTransaction->TransactionAmount                      = $b2c[1]['Value'];
        $b2cTransaction->B2CWorkingAccountAvailableFunds        = $b2c[2]['Value'];
        $b2cTransaction->B2CUtilityAccountAvailableFunds        = $b2c[3]['Value'];
        $b2cTransaction->TransactionCompletedDateTime           = $b2c[4]['Value'];
        $b2cTransaction->ReceiverPartyPublicName                = $b2c[5]['Value'];
        $b2cTransaction->B2CChargesPaidAccountAvailableFunds    = $b2c[6]['Value'];
        $b2cTransaction->B2CRecipientIsRegisteredCustomer       = $b2c[7]['Value'];

        $b2cTransaction->save(); */




  /*       b2cTransaction::create([
            'TransactionReceipt' => $resultB2C->Result->ResultParameters->ResultParameter[0]->Value,
            'TransactionAmount' => $resultB2C->Result->ResultParameters->ResultParameter[1]->Value,
            'B2CWorkingAccountAvailableFunds' => $resultB2C->Result->ResultParameters->ResultParameter[2]->Value,
            'B2CUtilityAccountAvailableFunds' => $resultB2C->Result->ResultParameters->ResultParameter[3]->Value,
            'TransactionCompletedDateTime' => $resultB2C->Result->ResultParameters->ResultParameter[4]->Value,
            'ReceiverPartyPublicName' => $resultB2C->Result->ResultParameters->ResultParameter[5]->Value,
            'B2CChargesPaidAccountAvailableFunds' => $resultB2C->Result->ResultParameters->ResultParameter[6]->Value,
            'B2CRecipientIsRegisteredCustomer' => $resultB2C->Result->ResultParameters->ResultParameter[7]->Value,
            ]); */


        Log::info($request->all());

        return [
            'ResultCode' => 0,
            'ResultDesc' => 'Accept Service',
            'ThirdPartyTransID' => rand(3000, 10000)
        ];
    }

}
