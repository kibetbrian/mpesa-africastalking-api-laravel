<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use AfricasTalking\SDK\AfricasTalking;
use \Savannabits\Advantasms\Advantasms;
use Exception;


class SmsController extends Controller
{
    public function sendSmsViaAfricasTaklikng(){

        // Set your app credentials
        $username       = "sandbox";
        $apiKey         = "cc9ff74fcd7a2c753424ef47fa62f50f1fe91152258d0ab15ae517c06a20f96a";

        $data['sms_body']   = "Hello";
        $data['mobile_number']     = "0727970853";

        // Initialize the SDK
        $AT = new AfricasTalking($username, $apiKey);

        // Get the SMS service
        $sms        = $AT->sms();
        $numbers = explode(',', trim($data['mobile_number']));

        foreach ($numbers as  $number) {
            $sms->send([
                'to' => $number,
                'message' => $data['sms_body'],
                'from' => '25008',
            ]);
        }
    }

}
