<?php

namespace App\Http\Controllers;

use Dompdf\Dompdf;
use App\MpesaPayments;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Traits\MpesaTrait;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\MpesaResource;
use App\Http\Resources\MpesaCollection;

class MpesaController extends Controller
{
    use MpesaTrait;

    public function generateAccessToken()
    {
        $consumerKey = 'otGWEGsziEGiG9NPW3i8c8kA2dH8wGhP';
        $consumerSecret = '8UfVZKKLolnrfzeE';

        $headers = ['Content-Type:application/json; charset=utf8'];

        $url = 'https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_HEADER, FALSE);
        curl_setopt($curl, CURLOPT_USERPWD, $consumerKey.':'.$consumerSecret);
        $result = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $result = json_decode($result);

        $access_token = $result->access_token;

        return $access_token;
    }

    public function registerUrl()
    {
        $url = 'https://sandbox.safaricom.co.ke/mpesa/c2b/v1/registerurl';

        $shortCode = '603021';
       /*  $confirmationUrl = 'https://dev1.hikecode.pw/api/validation';
        $validationUrl = 'https://dev1.hikecode.pw/api/confirmation';
 */
        $validationUrl  = 'https://7499068c6a97.ngrok.io/api/validation';
        $confirmationUrl = 'https://7499068c6a97.ngrok.io/api/confirmation';


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$this->generateAccessToken()));

        $curl_post_data = array(
          'ShortCode' => $shortCode,
          'ResponseType' => 'Confirmed',
          'ConfirmationURL' => $confirmationUrl,
          'ValidationURL' => $validationUrl
        );
        $data_string = json_encode($curl_post_data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        $curl_response = curl_exec($curl);

        return $curl_response;

    }

    public function simulateTransaction(Request $request)
    {
        $this->registerUrl();

        $data = $request->all();

        /* dd($data); */

        $url = 'https://sandbox.safaricom.co.ke/mpesa/c2b/v1/simulate';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '. $this->generateAccessToken()));
        $curl_post_data = array(
               'ShortCode' => 603021,
               'CommandID' => 'CustomerPayBillOnline',
               'Amount' => $data['Amount'],
               'Msisdn' => 254708374149,
               'BillRefNumber' => '34SY4'
        );

        $data_string = json_encode($curl_post_data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        $curl_response = curl_exec($curl);

        return $curl_response;
    }

    public function myMpesaPassword()
    {
        $lipa_time = Carbon::rawParse('now')->format('YmdHms');

        $passkey = "bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919";
        $BusinessShortCode = 174379;
        $timestamp =$lipa_time;
        $lipa_na_mpesa_password = base64_encode($BusinessShortCode.$passkey.$timestamp);

        return $lipa_na_mpesa_password;
    }

    public function mpesaSTKPush()
    {
        $url = 'https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$this->generateAccessToken()));
        $curl_post_data = [

            'BusinessShortCode' => 174379,
            'Password' => $this->myMpesaPassword(),
            'Timestamp' => Carbon::rawParse('now')->format('YmdHms'),
            'TransactionType' => 'CustomerPayBillOnline',
            'Amount' => 1,
            'PartyA' => 254727970853,
            'PartyB' => 174379,
            'PhoneNumber' => 254727970853,
            'CallBackURL' => 'https://dec868466269.ngrok.io/api/callback_url',
            'AccountReference' => "Brian Kiptanui",
            'TransactionDesc' => "Testing stk push on sandbox"
        ];
        $data_string = json_encode($curl_post_data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        $curl_response = curl_exec($curl);

        return $curl_response;
    }

    public function simulateB2C()
    {
        $InitiatorName = 'apiop37';
        $SecurityCredential = 'BxvUoe3ocQ6u1IgOVo5bBmRIkQz9bO1KYfNwv2z8DkKLoRKCaIm+TlrDyiGbrUNXmpoW5RUCKw+z1+WX+6yLNLnSyCCm5gET4h9UAN+mMAjC15ErRoJErIX4yKP5WGWn9JcsTbnJbBLEhX8Qffi40pQhrDpJM8mnpBUiJwr/RV6qQMiC/KggG7pKUmudpOZgdktQHw+JLSs0eVLCsAZtCV8R4esZ7nE58o5x6Rb6dQgsq6dGmSnTTaDo0cDCe1ThvSAEE2preKQvq6xr1JL9wRWwP66I7zkRvhKXxmFqPVTkIUUIe7znIrATdsgC6RoAm8jGK5B0SUjMs/ogLFi6qg==';
        $CommandID = 'SalaryPayment';
        $Amount = '20';
        $PartyA = '603021';
        $PartyB = '254708374149';
        $Remarks = 'Salary';
        $QueueTimeOutURL = 'https://dev1.hikecode.pw/api/timeoutB2C';
        $ResultURL = 'https://dev1.hikecode.pw/api/resultB2C';
        $Occasion = 'salary';

        $b2c_url = 'https://sandbox.safaricom.co.ke/mpesa/b2c/v1/paymentrequest';
        $b2cHeader = ['Content-Type:application/json','Authorization:Bearer '. $this->generateAccessToken()];
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $b2c_url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $b2cHeader);

        $curl_post_data = array(

          'InitiatorName' => $InitiatorName,
          'SecurityCredential' => $SecurityCredential,
          'CommandID' => $CommandID,
          'Amount' => $Amount,
          'PartyA' => $PartyA,
          'PartyB' => $PartyB,
          'Remarks' => $Remarks,
          'QueueTimeOutURL' => $QueueTimeOutURL,
          'ResultURL' => $ResultURL,
          'Occasion' => $Occasion
        );

        $data_string = json_encode($curl_post_data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        $curl_response = curl_exec($curl);

        return  $curl_response;
    }

    public function accountBalance()
    {
        $bal_url = 'https://sandbox.safaricom.co.ke/mpesa/accountbalance/v1/query';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $bal_url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer ' . $this->generateAccessToken()));

        $curl_post_data = array(

          'Initiator'           => 'apiop37',
          'SecurityCredential'  => 'BxvUoe3ocQ6u1IgOVo5bBmRIkQz9bO1KYfNwv2z8DkKLoRKCaIm+TlrDyiGbrUNXmpoW5RUCKw+z1+WX+6yLNLnSyCCm5gET4h9UAN+mMAjC15ErRoJErIX4yKP5WGWn9JcsTbnJbBLEhX8Qffi40pQhrDpJM8mnpBUiJwr/RV6qQMiC/KggG7pKUmudpOZgdktQHw+JLSs0eVLCsAZtCV8R4esZ7nE58o5x6Rb6dQgsq6dGmSnTTaDo0cDCe1ThvSAEE2preKQvq6xr1JL9wRWwP66I7zkRvhKXxmFqPVTkIUUIe7znIrATdsgC6RoAm8jGK5B0SUjMs/ogLFi6qg==',
          'CommandID'           => 'AccountBalance',
          'PartyA'              => '603021',
          'IdentifierType'      => '4',
          'Remarks'             => 'balance',
          'QueueTimeOutURL'     => 'https://dec868466269.ngrok.io/api/callback_url',
          'ResultURL'           => 'https://dec868466269.ngrok.io/api/callback_url'
        );

        $data_string = json_encode($curl_post_data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        $curl_response = curl_exec($curl);

        return $curl_response;
    }

    public function getData()
    {

        $data = DB::table('mpesa_payments')->get();
        //$data = MpesaPayments::all();

        //$data = DB::table('mpesa_payments')->pluck('TransAmount');
        //$data = DB::table('mpesa_payments')->sum('TransAmount');
        // $data = MpesaPayments::whereBetween('TransTime', array('2021-05-13', '2021-05-12'))
        //         ->sum('TransAmount');
       // $data = MpesaPayments::whereNotIn('TransID', array('PEC01HJ63E', 'PEC11HJ63F'))->sum('TransAmount');
    //    $data = DB::table('mpesa_payments')
    //             ->whereBetween('TransTime', array('2021-05-12', '2021-05-13'))
    //             ->whereNotIn('TransID', array('PEC01HJ63E', 'PEC11HJ63F', 'PEC31HJ62X'))
    //             ->sum('TransAmount');
        /* $data = DB::table('mpesa_payments')
            ->select(DB::raw('sum(TransAmount) as total'))->get();

        $data = DB::table('mpesa_payments')
            ->select('TransTime', DB::raw('sum(TransAmount) as total'))
            ->groupBy('TransTime')
            ->get();

        $data = MpesaPayments::groupBy('id')
            ->selectRaw('sum(TransAmount) as total')
            ->get();
        return $data; */

       // return $data;

       // return new MpesaResource($data);

    return response()->json(new MpesaCollection($data));
       // return MpesaResource::collection(MpesaPayments::all());
    }

    public function showMpesaData(){
        $data = MpesaPayments::all();
        return view('pdf/pdf', compact('data'));
      }

    // public function export_pdf()
    // {
    //   $data = MpesaPayments::get();

    //   $pdf = PDF::loadView('pdf.pdf', compact('data'));

    //   $pdf->save(storage_path().'_filename.pdf');

    //   return $pdf->download('pdf.pdf');
    // }

    public function export_pdf()
    {
    $data = MpesaPayments::get(); 
    $total = MpesaPayments::get()->sum('TransAmount');


    // $pdf = PDF::loadView('pdf_view', compact('data', 'total'))->setPaper('L', 'landscape');

    // $pdf->save(storage_path().'_filename.pdf');

    // $pdf->output();
    // $dom_pdf = $pdf->getDomPDF();

    // $canvas = $dom_pdf ->get_canvas();
    // $canvas->page_text(2, 2, "Page {PAGE_NUM} of {PAGE_COUNT}", null, 10, array(0, 0, 0));

    // return $pdf->download('pdf_file.pdf');

    $dompdf = new Dompdf();

    $html ='<!doctype html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <style>
            /* Define the margins of your page */
            @page {
                margin: 100px 25px;
            }
            .text-center mb-3{
                margin-left:180px;
            }
            header {
                position: fixed;
                top: -60px;
                left: 0px;
                right: 0px;
                height: 50px;

                /* Extra personal styles */
                background-color: white;
                color: black;
                text-align: center;
                line-height: 35px;
            }

            footer {
                position: fixed;
                bottom: -60px;
                left: 0px;
                right: 0px;
                height: 50px;

                /* Extra personal styles */
                background-color: white;
                color: black;
                text-align: center;
                line-height: 35px;
            }
            table {
                width: 750px;
                border-collapse: collapse;
                margin:50px auto;
                }
            tr:nth-of-type(odd) {
                background: #eee;
                }

            th {
                background: white;
                color: black;
                font-weight: bold;
                }

            td, th {
                padding: 10px;
                border: 1px solid #ccc;
                text-align: left;
                font-size: 13px;
                }
            @media
            only screen and (max-width: 760px),
            (min-device-width: 768px) and (max-device-width: 1024px)  {

                table {
                    width: 120%;
                }
                table, thead, tbody, th, td, tr {
                    display: block;
                }
                thead tr {
                    position: absolute;
                    top: -9999px;
                    left: -9999px;
                }

                tr { border: 1px solid #ccc; }

                td {
                    border: none;
                    border-bottom: 1px solid #eee;
                    position: relative;
                    padding-left: 50%;
                }

                td:before {
                    position: absolute;
                    top: 6px;
                    left: 6px;
                    width: 45%;
                    padding-right: 10px;
                    white-space: nowrap;
                    content: attr(data-column);
                    color: #000;
                    font-weight: bold;
                }
            }
        </style>

    </head>
    <body>

    <h2 style="margin-left:300px;">Ruaka Biz</h2>
    <p style="margin-left:300px;">Monthly Report</p>
    <footer>
        Copyright &copy; .2021 Hikecode Ltd
    </footer>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr class="table">
                    <th>#</th>
                    <th>Time</th>
                    <th>Transaction ID</th>
                    <th>Phone</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Amount</th>
                </tr>
            </thead>';

                foreach($data as $key => $pdf){
                    $html .='
                    <tbody>
                    <tr>

                        <th scope="row"> '.++$key.'.</th>
                        <td> '.$pdf->TransTime.' </td>
                        <td> '.$pdf->TransID.'</td>
                        <td> '.$pdf->MSISDN.'</td>
                        <td> '.$pdf->FirstName.' </td>
                        <td> '.$pdf->LastName.' </td>
                        <td> '.$pdf->TransAmount.' </td>
                    </tr>
                    </tbody>
                    ';
                }
                $html .= '<tbody><td colspan="6"><td>Total :  '.$total.' </td></td></tbody>';

                $html .= '
            </table>
            </div>
          </body>
        </html>';

    $dompdf->loadHTML($html);
    $dompdf->setPaper('A4', 'portrait');
    $dompdf->render();

    // Parameters
    $x          = 505;
    $y          = 790;
    $text       = "{PAGE_NUM} of {PAGE_COUNT}";
    $font       = $dompdf->getFontMetrics()->get_font('Helvetica', 'normal');
    $size       = 10;
    $color      = array(0,0,0);
    $word_space = 0.0;
    $char_space = 0.0;
    $angle      = 0.0;

    $dompdf->getCanvas()->page_text(
    $x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle
    );

    // stream
    $dompdf->stream('pdf_out.pdf', array('Attachment' => true));

    }
}


