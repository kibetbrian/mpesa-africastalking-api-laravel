<?php

namespace App\Http\Traits;

use AfricasTalking\SDK\AfricasTalking;

trait SendSmsTrait
{
    public function sendSms($mobile, $message)
    {
        $username = 'sandbox';
        $apiKey   = '0dacc36fb6a9d6b930d9668a8b97cd93e2eb436e94db07cfb2804e3073b6a7cf';
        $AT       = new AfricasTalking($username, $apiKey);
        $sms      = $AT->sms();

        $numbers = explode(',', trim($mobile));
        foreach ($numbers as $number) {
            $sms->send([
                'to'      => $number,
                'message' => $message,
                'from' => '2566',
                'unqueue' => 'true'
            ]);
        }
    }
}
?>
