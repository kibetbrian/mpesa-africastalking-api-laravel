<?php

namespace App\Http\Traits;

trait MpesaTrait
{
    public function mpesaTrait($url, $headers, $post_string)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_POSTFIELDS,  $post_string);

        $data = curl_exec($curl);
        return  $data;
    }
}

?>
