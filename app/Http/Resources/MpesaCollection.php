<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\DB;

class MpesaCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            //'FirstName' => $this->FirstName,
            //'TransAmount' => $this->TransAmount,
            //$data = DB::table('mpesa_payments')->pluck('TransAmount'),
            'data' => $this->collection,
            'total' => $this->collection->sum('TransAmount'),
            //'OrganisationalTotal' => $this->collection->sum('OrgAccountBalance')
        ];
    }
}
