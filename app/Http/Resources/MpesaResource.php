<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MpesaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

            return [
                'FirstName' => $this->FirstName,
                'total' => $this->sum('TransAmount'),
                //'GrandTotal' => $this->collection->sum('TransAmount')
            ];
    }
}
