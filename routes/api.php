<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('access/token', 'MpesaController@generateAccessToken');
Route::post('register', 'MpesaController@registerUrl');
Route::post('simulation', 'MpesaController@simulateTransaction');
Route::post('lipa', 'MpesaController@mpesaSTKPush');
Route::post('simulate/B2C', 'MpesaController@simulateB2C');
Route::post('account_balance', 'MpesaController@accountBalance');

Route::post('callback_url', 'MpesaResponsesController@callbackURL');
Route::post('validation', 'MpesaResponsesController@validationUrl');
Route::post('confirmation', 'MpesaResponsesController@confirmationUrl');
Route::post('resultB2C', 'MpesaResponsesController@resultURL');

Route::get('get-data', 'MpesaController@getData');
Route::get('export_pdf', 'MpesaController@export_pdf');


/* Route::post('token', 'DarajaController@getAccessToken');
Route::post('register1', 'DarajaController@registerURL');
Route::post('confirmation1', 'DarajaResponsesController@ConfirmationURL');
Route::post('validation1', 'DarajaResponsesController@ValidationURL');
Route::post('simulation_c2b', 'DarajaController@SimulateC2B'); */

// Route::post('sendSms', 'SmsController@sendSmsViaAfricasTaklikng');
