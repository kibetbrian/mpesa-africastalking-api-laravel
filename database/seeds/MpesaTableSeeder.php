<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MpesaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mpesa_payments')->insert([
            'FirstName' => 'Brian',
            'MiddleName' => 'Kiptanui',
            'LastName' => 'Kim',
            'TransactionType' => 'PayBill',
            'TransID' => 'PEC41HJ62Y',
            'TransTime' => '2021-05-12',
            'BusinessShortCode' => '6996634',
            'BillRefNumber' => 'GUU99',
            'MSISDN' => '0727970853',
            'TransAmount' => '2457',
            'OrgAccountBalance' => '56774',
        ]);
    }
}
