<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateB2cTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b2c_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('TransactionReceipt')->nullable();
            $table->string('TransactionCompletedDateTime')->nullable();
            $table->string('TransactionAmount')->nullable();
            $table->string('B2CWorkingAccountAvailableFunds')->nullable();
            $table->string('B2CRecipientIsRegisteredCustomer')->nullable();
            $table->string('B2CUtilityAccountAvailableFunds')->nullable();
            $table->string('B2CChargesPaidAccountAvailableFunds')->nullable();
            $table->string('ReceiverPartyPublicName')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b2c_transactions');
    }
}
