<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSTKpushesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_t_kpushes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('Amount')->nullable();
            $table->string('TransactionDate')->nullable();
            $table->string('MpesaReceiptNumber')->nullable();
            $table->string('Balance')->nullable();
            $table->string('PhoneNumber')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_t_kpushes');
    }
}
