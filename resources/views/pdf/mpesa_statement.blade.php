{{--  
<a href="{{ URL::to('pdf') }}">mpesa pdf</a>

<div>
    <table>
        <thead>
          <tr>
              <th>#</th>
              <th>Time</th>
              <th>Transaction ID </th>
              <th>Phone</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Amount</th>
          </tr>
        </thead>
        <tbody>
          @foreach($data as $pdf)
            <tr>
              <td>{{ $pdf->id }}</td>
              <td>{{ $pdf->TransTime }}</td>
              <td>{{ $pdf->TransID }}</td>
              <td>{{ $pdf->MSISDN }}</td>
              <td>{{ $pdf->FirstName }}</td>
              <td>{{ $pdf->LastName }}</td>
              <td>{{ $pdf->TransAmount }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
      <table> <thead><td colspan="6">Total:{{ $pdf->sum('TransAmount') }}</td><thead></table>
</div>
  --}}

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laravel 7 PDF Example</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
</head>

<body>
    <div class="container mt-5">
        <h2 class="text-center mb-3">Laravel HTML to PDF Example</h2>

        <div class="d-flex justify-content-end mb-4">
            <a class="btn btn-primary" href="{{ URL::to('pdf') }}">Export to PDF</a>
        </div>

        <table class="table table-bordered mb-5">
            <thead>
                <tr class="table-danger">
                    <th scope="col">#</th>
                    <th scope="col">Time</th>
                    <th scope="col">TransTime</th>
                    <th scope="col">Phone</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Amount</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $pdf)
                <tr>
                
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>{{ $pdf->TransTime }}</td>
                    <td>{{ $pdf->TransID }}</td>
                    <td>{{ $pdf->MSISDN }}</td>
                    <td>{{ $pdf->FirstName }}</td>
                    <td>{{ $pdf->LastName }}</td>
                    <td>{{ $pdf->TransAmount }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>

    <script src="{{ asset('js/app.js') }}" type="text/js"></script>
</body>

</html>


