
## About Project

The application has all the api's from Daraja, that is including c2b, b2c, b2b and stk-push.
Also it has implimentation for Africastalking Api's

## Installation

1. Clone the project to your machine. 

```
$ git clone https://gitlab.com/kibetbrian/mpesa-africastalking-api-laravel.git

```  

2. Install composer packages

```
$ composer install OR 
$ composer update

```  
3. Create .env file
4. Create your database
5. Migrate your database 

```
$ php artisan migrate

``` 


6. Start laravel local server  then test your Api's

```
$ php artisan serve

``` 

## Contact

- Name: Brian Kiptanui.
- Email: kimutaibrian9@gmail.com

